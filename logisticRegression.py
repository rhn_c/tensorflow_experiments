# These are all the modules we'll be using later. Make sure you can import them
# before proceeding further.
from __future__ import print_function
import matplotlib.pyplot as plt
import numpy as np
import os
import sys
import tarfile
import hashlib
from IPython.display import display, Image
from scipy import ndimage
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import zero_one_loss, classification_report
from six.moves.urllib.request import urlretrieve
from six.moves import cPickle as pickle


#View a set of images from a pickle dump
def viewFolderSamples(pdump):	

    print("loading: " + pdump)
    data = pickle.load(open(pdump,"rb"))

    print("loaded")
    for f in data:
        print ("displaying")
        plt.imshow(f)
        plt.show()
        print("onscreen")

#Construct and return an array with allocated space for [nb_rows] elements of [img_size]x[img_size]
#@Params: 
#	 nb_rows - the number of images to be stored in the array
#	 img_size - the pixel size of each image(assumes images are square)
#@Return: an array of [nb_rows] many images
def make_arrays(nb_rows, img_size):
    if nb_rows:
        dataset = np.ndarray((nb_rows, img_size, img_size), dtype=np.float32)
        labels = np.ndarray(nb_rows, dtype=np.int32)
    else:
        dataset, labels = None, None
    return dataset, labels

#Takes the datasets for each digit, merge them into validation and training sets, shuffle them, and then return the sets
#@Params:
#	 pickle_files - the pickled datasets that will be combined into training/validation/testing sets
#	 train_size - the number of images to include in the training set
#	 valid_size - the number of images to include in the validation set
#@Return: Returns the validation dataset and an array of labels that indicate the true class of each validation image in the dataset
#		similarly returns a training dataset and associated labels
def merge_datasets(pickle_files, train_size, valid_size=0):
    num_classes = len(pickle_files)
    valid_dataset, valid_labels = make_arrays(valid_size, image_size)
    train_dataset, train_labels = make_arrays(train_size, image_size)
    vsize_per_class = valid_size // num_classes
    tsize_per_class = train_size // num_classes
      
    start_v, start_t = 0, 0
    end_v, end_t = vsize_per_class, tsize_per_class
    end_l = vsize_per_class+tsize_per_class
    for label, pickle_file in enumerate(pickle_files):       
        try:
            with open(pickle_file, 'rb') as f:
                letter_set = pickle.load(f)
                # shuffle the letters to have random validation and training set
                np.random.shuffle(letter_set)
                if valid_dataset is not None:
                    valid_letter = letter_set[:vsize_per_class, :, :]
                    valid_dataset[start_v:end_v, :, :] = valid_letter
                    valid_labels[start_v:end_v] = label
                    start_v += vsize_per_class
                    end_v += vsize_per_class
                            
                train_letter = letter_set[vsize_per_class:end_l, :, :]
                train_dataset[start_t:end_t, :, :] = train_letter
                train_labels[start_t:end_t] = label
                start_t += tsize_per_class
                end_t += tsize_per_class
        except Exception as e:
            print('Unable to process data from', pickle_file, ':', e)
            raise
        
    return valid_dataset, valid_labels, train_dataset, train_labels

#shuffles a given dataset but makes sure the associated label for each image is correctly associated 
def randomize(dataset, labels):
    permutation = np.random.permutation(labels.shape[0])
    shuffled_dataset = dataset[permutation,:,:]
    shuffled_labels = labels[permutation]
    return shuffled_dataset, shuffled_labels



#Load the data for a single letter label and return its image data in a numpy array
#@Params: 
#	 folder - the name of the folder whose images will be loaded
#	 min_num_images - the minimum number of images expected in the folder
#@Return a numpy array containing the images from the desired folder
def load_letter(folder, min_num_images):
    image_files = os.listdir(folder)
    dataset = np.ndarray(shape=(len(image_files), image_size, image_size),
                         dtype=np.float32)
    image_index = 0
    print(folder)
    for image in os.listdir(folder):
        image_file = os.path.join(folder, image)
        try:
            image_data = (ndimage.imread(image_file).astype(float) - 
                          pixel_depth / 2) / pixel_depth
            if image_data.shape != (image_size, image_size):
                raise Exception('Unexpected image shape: %s' % str(image_data.shape))
            dataset[image_index, :, :] = image_data
            image_index += 1
        except IOError as e:
            print('Could not read:', image_file, ':', e, '- it\'s ok, skipping.')
      
    num_images = image_index
    dataset = dataset[0:num_images, :, :]
    if num_images < min_num_images:
        raise Exception('Many fewer images than expected: %d < %d' %
                      (num_images, min_num_images))
      
    print('Full dataset tensor:', dataset.shape)
    print('Mean:', np.mean(dataset))
    print('Standard deviation:', np.std(dataset))
    return dataset
       
#take a given data set and pickle it and store to disk if it has not been done for the data already
#otherwise load the pickled dataset. In both cases, return the names of the datasets processed
def maybe_pickle(data_folders, min_num_images_per_class, force=False):
    dataset_names = []
    for folder in data_folders:
        set_filename = folder + '.pickle'
        dataset_names.append(set_filename)
        if os.path.exists(set_filename) and not force:
            # You may override by setting force=True.
            print('%s already present - Skipping pickling.' % set_filename)
        else:
            print('Pickling %s.' % set_filename)
            dataset = load_letter(folder, min_num_images_per_class)
            try:
                with open(set_filename, 'wb') as f:
                    pickle.dump(dataset, f, pickle.HIGHEST_PROTOCOL)
            except Exception as e:
                print('Unable to save data to', set_filename, ':', e)
    return dataset_names


#Removes duplicate examples that appear in the training set and the validation/testing set. Returns the cleaned validation
#and test datasets with associated labels
#@Params: 
#	 train_dataset 	- the images in the training set
#	 valid_dataset 	- the images in the validation set
#	 test_dataset 	- the images in the testing set
#	 pickle_file 	- location to store the clean datasets, to avoid recomputation in future runs
#	 force		- forces the recomputation of the clean datasets and resaves them
#@Return: returns the cleaned validation and test sets, such that they contain no images that are also in the training set
def removeDuplicates(train_dataset,valid_dataset,test_dataset, pickle_file = 'notMNIST_no_overlap.pickle', force = False):

	if os.path.exists(pickle_file) and not force:
		print ("Pickled cleaned data set present at ", pickle_file, ": loading and continuing")
		with open (pickle_file,"rb") as fi:
			data = pickle.load(fi)
			valid_dataset_clean = data['valid_dataset_clean']
                        valid_labels_clean  = data['valid_labels_clean']
                        test_dataset_clean  = data['test_dataset_clean']
                        test_labels_clean   = data['test_labels_clean']
			del data
	else:
		#compute a hash on the data of each image, use this for comparing and detecting duplicate images
		train_hashes = [hashlib.sha1(x).digest() for x in train_dataset]
		valid_hashes = [hashlib.sha1(x).digest() for x in valid_dataset]
		test_hashes  = [hashlib.sha1(x).digest() for x in test_dataset]

		#find all samples that appear in both [set1] and [set2]
		valid_in_train = np.in1d(valid_hashes, train_hashes)
		test_in_train  = np.in1d(test_hashes,  train_hashes)
		test_in_valid  = np.in1d(test_hashes,  valid_hashes)

		#mask of indicies that do not appear in multiple sets of train/test/valid
		valid_keep = ~valid_in_train
		test_keep  = ~(test_in_train | test_in_valid)

		#clean set consists of only non duplicates (i.e those that are in the mask)
		valid_dataset_clean = valid_dataset[valid_keep]
		valid_dataset_clean = valid_dataset_clean.reshape(np.shape(valid_dataset_clean)[0],-1)
		valid_labels_clean  = valid_labels [valid_keep]

		test_dataset_clean = test_dataset[test_keep]
		test_dataset_clean = test_dataset_clean.reshape(np.shape(test_dataset_clean)[0],-1)
		test_labels_clean  = test_labels [test_keep]

		print("valid -> train overlap: %d samples" % valid_in_train.sum())
		print("test  -> train overlap: %d samples" % test_in_train.sum())
		print("test  -> valid overlap: %d samples" % test_in_valid.sum())

		#store the clean validation/test sets

		try:
			f = open(pickle_file, 'wb')
			save = {
			  'valid_dataset_clean': valid_dataset_clean,
			  'valid_labels_clean': valid_labels_clean,
			  'test_dataset_clean': test_dataset_clean,
			  'test_labels_clean': test_labels_clean,
			  }
			pickle.dump(save, f, pickle.HIGHEST_PROTOCOL)
			f.close()
		except Exception as e:
			print('Unable to save data to', pickle_file, ':', e)
			raise

	return valid_dataset_clean, valid_labels_clean, test_dataset_clean, test_labels_clean
#--------------------------------------------------------------------------------------------

#Training/test folders must be generated from setup.py
train_folders = ['notMNIST_large/A', 'notMNIST_large/B', 'notMNIST_large/C', 'notMNIST_large/D', 'notMNIST_large/E', 'notMNIST_large/F', 'notMNIST_large/G', 'notMNIST_large/H', 'notMNIST_large/I', 'notMNIST_large/J']

test_folders = ['notMNIST_small/A', 'notMNIST_small/B', 'notMNIST_small/C', 'notMNIST_small/D', 'notMNIST_small/E', 'notMNIST_small/F', 'notMNIST_small/G', 'notMNIST_small/H', 'notMNIST_small/I', 'notMNIST_small/J']

num_classes = 10	#One class per digit
np.random.seed(133)	#Fixed seed for reproducability
image_size = 28  	#Pixel width and height.
pixel_depth = 255.0  	#Number of levels per pixel.



train_datasets = maybe_pickle(train_folders, 45000)
test_datasets = maybe_pickle(test_folders, 1800)


train_size = 200000
valid_size = 10000
test_size = 10000

valid_dataset, valid_labels, train_dataset, train_labels = merge_datasets(
  train_datasets, train_size, valid_size)
_, _, test_dataset, test_labels = merge_datasets(test_datasets, test_size)

print('Training:', train_dataset.shape, train_labels.shape)
print('Validation:', valid_dataset.shape, valid_labels.shape)
print('Testing:', test_dataset.shape, test_labels.shape)

#randomize the datasets
train_dataset, train_labels = randomize(train_dataset, train_labels)
test_dataset, test_labels = randomize(test_dataset, test_labels)
valid_dataset, valid_labels = randomize(valid_dataset, valid_labels)

#store these datasets for future use
pickle_file = 'notMNIST.pickle'
if not os.path.exists(pickle_file):
	try:
		f = open(pickle_file, 'wb')
		save = {
			'train_dataset': train_dataset,
			'train_labels': train_labels,
			'valid_dataset': valid_dataset,
			'valid_labels': valid_labels,
			'test_dataset': test_dataset,
			'test_labels': test_labels,
			}
		pickle.dump(save, f, pickle.HIGHEST_PROTOCOL)
		f.close()
	except Exception as e:
		print('Unable to save data to', pickle_file, ':', e)
		raise

#remove duplicates
valid_dataset_clean, valid_labels_clean, test_dataset_clean, test_labels_clean = removeDuplicates(train_dataset,valid_dataset,test_dataset, 'notMNIST_no_overlap.pickle', False)

#Initialize the logisticRegression method
logreg = LogisticRegression(random_state=413, multi_class='multinomial', solver='newton-cg')

#Print some diagnositic information as a sanity check
print ("Training set size: ")
print (np.shape(train_dataset))
print (np.shape(train_labels))

print ("Validation set size:")
print (np.shape(valid_dataset_clean))
print (np.shape(valid_labels_clean))


print ("Test set size:")
print (np.shape(test_dataset_clean))
print (np.shape(test_labels_clean))

print ("Start Training:")

#Test the logisticRegression classifier using different amounts of training data and then report some statistics
for nSample in [50, 100, 1000, 5000]:
	print ("Training with ", nSample, " examples in training set")
	logreg.fit(train_dataset[:nSample].reshape(nSample,-1),train_labels[:nSample])
	pred = logreg.predict(test_dataset_clean)

	error_rate = zero_one_loss(test_labels_clean,pred)

	print ( 'Error rate: %.2f%%' % (error_rate*100.0))
	print ( classification_report(test_labels_clean, pred, target_names=list('ABCDEFGHIJ')))






