# These are all the modules we'll be using later. Make sure you can import them
# before proceeding further.
from __future__ import print_function
import numpy as np
import tensorflow as tf
from six.moves import cPickle as pickle
from six.moves import range


image_size = 28
num_labels = 10

#Reshape the image data as a flat matrix, reshape labels as 1-hot encodings
def reformat(dataset, labels):
	dataset = dataset.reshape((-1, image_size * image_size)).astype(np.float32)
	# Map 0 to [1.0, 0.0, 0.0 ...], 1 to [0.0, 1.0, 0.0 ...]
	labels = (np.arange(num_labels) == labels[:,None]).astype(np.float32)
	return dataset, labels

def accuracy(predictions, labels):
  return (100.0 * np.sum(np.argmax(predictions, 1) == np.argmax(labels, 1))
          / predictions.shape[0])


#-----------------------------------------------------------------------------------------------------------------
pickle_file 	= 'notMNIST.pickle'
batch_size 		= 128
train_subset 	= 10000 	#reduce this to achieve overfitting
graph			= tf.Graph()
num_steps 		= 10000 #95001
H1_layer_size 	= 1024
H2_layer_size	= 300
H3_layer_size	= 75
multilayer 		= True



with open(pickle_file, 'rb') as f:
	save = pickle.load(f)
	train_dataset = save['train_dataset']
	train_labels = save['train_labels']
	valid_dataset = save['valid_dataset']
	valid_labels = save['valid_labels']
	test_dataset = save['test_dataset']
	test_labels = save['test_labels']
	del save  # hint to help gc free up memory
	print('Training set', train_dataset.shape, train_labels.shape)
	print('Validation set', valid_dataset.shape, valid_labels.shape)
	print('Test set', test_dataset.shape, test_labels.shape)

#Reshape the data
train_dataset, train_labels = reformat(train_dataset, train_labels)
valid_dataset, valid_labels = reformat(valid_dataset, valid_labels)
test_dataset, test_labels = reformat(test_dataset, test_labels)
print('Training set', train_dataset.shape, train_labels.shape)
print('Validation set', valid_dataset.shape, valid_labels.shape)
print('Test set', test_dataset.shape, test_labels.shape)

beta = 0.03
with graph.as_default():

	# Input data. For the training data, we use a placeholder that will be fed
	# at run time with a training minibatch.
	tf_train_dataset = tf.placeholder(tf.float32,
					  shape=(batch_size, image_size * image_size))
	tf_train_labels = tf.placeholder(tf.float32, shape=(batch_size, num_labels))
	tf_valid_dataset = tf.constant(valid_dataset)
	tf_test_dataset = tf.constant(test_dataset)


	#>>>>>>>>>>>>>>Hidden layer<<<<<<<<<<

	keep_prob = 0.5

	#want to connect image pixels to our hidden layer of relu units with drop out applied
	w_fc1 = tf.Variable(tf.truncated_normal([image_size * image_size, H1_layer_size], stddev=0.0517))
	b_fc1 = tf.Variable(tf.zeros([H1_layer_size]))
	h_fc1 = tf.nn.relu(tf.matmul(tf_train_dataset,w_fc1) + b_fc1) 
	h_fc1_drop = tf.nn.dropout(h_fc1,keep_prob)
	last_layer_size = H1_layer_size

	if multilayer:
		#second hidden layer
		w_fc2 = tf.Variable(tf.truncated_normal([H1_layer_size, H2_layer_size], stddev=0.045))
		b_fc2 = tf.Variable(tf.zeros([H2_layer_size]))
		h_fc2 = tf.nn.relu(tf.matmul(h_fc1,w_fc2) + b_fc2)
		h_fc2_drop = tf.nn.dropout(h_fc2,keep_prob)
		#third hidden layer
		w_fc3 = tf.Variable(tf.truncated_normal([H2_layer_size, H3_layer_size], stddev=0.045))
		b_fc3 = tf.Variable(tf.zeros([H3_layer_size]))
		h_fc3 = tf.nn.relu(tf.matmul(h_fc2,w_fc3) + b_fc3)
		h_fc3_drop = tf.nn.dropout(h_fc3,keep_prob)

		last_layer_size = H3_layer_size


#connect the hidden layer to the output layer
	w_o = tf.Variable(tf.truncated_normal([last_layer_size, num_labels], stddev=0.08))
	b_o = tf.Variable(tf.zeros([num_labels]))
#input to this layer is a seperate weight matrix applied to the output of the relu units in the hidden layer	

	if not multilayer:
		logits = tf.matmul(h_fc1_drop, w_o) + b_o
		regularization = beta*(tf.nn.l2_loss(w_fc1))
	else:
		logits = tf.matmul(h_fc3_drop, w_o) + b_o
		#compute the l2 regularization (penalize large weights)
		regularization = beta*(tf.nn.l2_loss(w_fc3) + tf.nn.l2_loss(w_fc2) + tf.nn.l2_loss(w_fc1)) +(beta)*tf.nn.l2_loss(w_o)

	# Training computation:
	loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits, tf_train_labels)) + regularization

# Optimizer.
#		optimizer = tf.train.GradientDescentOptimizer(0.5).minimize(loss)
	global_step = tf.Variable(0)  # count the number of steps taken.
	learning_rate = tf.train.exponential_decay(0.3, global_step,3500,0.86,staircase=True)
	optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss, global_step=global_step)

	# Predictions for the training, validation, and test data.
	train_prediction = tf.nn.softmax(logits)
	#note that the computation for validation and test must be changed to reflect 
	#the inclusion of the hidden layer of relu units
	if not multilayer:

			valid_logitsH = tf.nn.relu(tf.matmul(tf_valid_dataset,w_fc1) + b_fc1)
			valid_prediction = tf.nn.softmax(tf.matmul(valid_logitsH, w_o) + b_o)


			test_logitsH = tf.nn.relu(tf.matmul(tf_test_dataset,w_fc1) + b_fc1)
			test_prediction = tf.nn.softmax(tf.matmul(test_logitsH, w_o) + b_o)
	else:
			valid_logitsH1 = tf.nn.relu(tf.matmul(tf_valid_dataset,w_fc1) + b_fc1)
			valid_logitsH2 = tf.nn.relu(tf.matmul(valid_logitsH1,w_fc2) + b_fc2)
			valid_logitsH3 = tf.nn.relu(tf.matmul(valid_logitsH2,w_fc3) + b_fc3)
			valid_prediction = tf.nn.softmax(tf.matmul(valid_logitsH3, w_o) + b_o)

			test_logitsH1 = tf.nn.relu(tf.matmul(tf_test_dataset,w_fc1) + b_fc1)
			test_logitsH2 = tf.nn.relu(tf.matmul(test_logitsH1,w_fc2) + b_fc2)
			test_logitsH3 = tf.nn.relu(tf.matmul(test_logitsH2,w_fc3) + b_fc3)
			test_prediction = tf.nn.softmax(tf.matmul(test_logitsH3, w_o) + b_o)


with tf.Session(graph=graph) as session:
	tf.initialize_all_variables().run()
	print("Initialized")
	for step in range(num_steps):
		# Pick an offset within the training data, which has been randomized.
		# Note: we could use better randomization across epochs.
		offset = (step * batch_size) % (train_labels.shape[0] - batch_size)
		# Generate a minibatch.
		batch_data = train_dataset[offset:(offset + batch_size), :]
		batch_labels = train_labels[offset:(offset + batch_size), :]
		# Prepare a dictionary telling the session where to feed the minibatch.
		# The key of the dictionary is the placeholder node of the graph to be fed,
		# and the value is the numpy array to feed to it.
		feed_dict = {tf_train_dataset : batch_data, tf_train_labels : batch_labels}
		_, l, predictions = session.run(
		  [optimizer, loss, train_prediction], feed_dict=feed_dict)
		if (step % 500 == 0):
			print("Minibatch loss at step %d: %f" % (step, l))
			print("Minibatch accuracy: %.1f%%" % accuracy(predictions, batch_labels))
			print("Validation accuracy: %.1f%%" % accuracy(
			  valid_prediction.eval(), valid_labels))
	print("Test accuracy: %.1f%%" % accuracy(test_prediction.eval(), test_labels))








