# These are all the modules we'll be using later. Make sure you can import them
# before proceeding further.
from __future__ import print_function
import numpy as np
import tensorflow as tf
from six.moves import cPickle as pickle
from six.moves import range
import numpy as np



def reformat(dataset, labels):
	dataset = dataset.reshape(
	  (-1, image_size, image_size, num_channels)).astype(np.float32)
	labels = (np.arange(num_labels) == labels[:,None]).astype(np.float32)
	return dataset, labels

def accuracy(predictions, labels):
	return (100.0 * np.sum(np.argmax(predictions, 1) == np.argmax(labels, 1))
          / predictions.shape[0])


#----------------------------------Model Definition--------------------------------------------------------

def model(data, train):


	if model_sml:
		#The default model configuration
		conv = tf.nn.conv2d(data, layer1_weights, [1, 2, 2, 1], padding='SAME')
		hidden = tf.nn.relu(conv + layer1_biases)
		conv = tf.nn.conv2d(hidden, layer2_weights, [1, 2, 2, 1], padding='SAME')
		hidden = tf.nn.relu(conv + layer2_biases)
		shape = hidden.get_shape().as_list()
		reshape = tf.reshape(hidden, [shape[0], shape[1] * shape[2] * shape[3]])
		hidden = tf.nn.relu(tf.matmul(reshape, layer3_weights) + layer3_biases) 

		return tf.matmul(hidden, layer4_weights) + layer4_biases


	else:
		#regularized/larger model
		keep_prob = 0.6
		conv = tf.nn.conv2d(data, layer1_weights, [1, 1, 1, 1], padding='SAME')
		hidden = tf.nn.relu(conv + layer1_biases)
		pool = tf.nn.max_pool(hidden, [1, 2, 2, 1], [1, 2, 2, 1], padding='SAME')

		conv = tf.nn.conv2d(pool, layer2_weights, [1, 1, 1, 1], padding='SAME')
		hidden = tf.nn.relu(conv + layer2_biases)
		pool = tf.nn.max_pool(hidden, [1, 2, 2, 1], [1, 2, 2, 1], padding='SAME')
		
		shape = pool.get_shape().as_list()
		reshape = tf.reshape(pool, [shape[0], shape[1] * shape[2] * shape[3]])
		h_fc1 = tf.nn.relu(tf.matmul(reshape, layer3_weights) + layer3_biases) 
		if (train):
			h_fc1 = tf.nn.dropout(h_fc1,keep_prob)
		return tf.matmul(h_fc1, layer4_weights) + layer4_biases


#--------------------------Main script----------------------------

image_size = 28
num_labels = 10
num_channels = 1 # grayscale

model_sml = False #control param to switch between basic and pool/regularization model
if (model_sml):
	#The default model configuration
	batch_size = 16
	patch_size = 5
	depth = 16
	num_steps=3001
	num_hidden = 64

else:
	#regularized/larger model
	batch_size = 64
	patch_size = 5
	depth = 28
	num_steps = 9001
	H1_layer_size 	= 128
	beta = 5e-5 


pickle_file = 'notMNIST.pickle'

with open(pickle_file, 'rb') as f:
  save = pickle.load(f)
  train_dataset = save['train_dataset']
  train_labels = save['train_labels']
  valid_dataset = save['valid_dataset']
  valid_labels = save['valid_labels']
  test_dataset = save['test_dataset']
  test_labels = save['test_labels']
  del save  # hint to help gc free up memory
  print('Training set', train_dataset.shape, train_labels.shape)
  print('Validation set', valid_dataset.shape, valid_labels.shape)
  print('Test set', test_dataset.shape, test_labels.shape)



train_dataset, train_labels = reformat(train_dataset, train_labels)
valid_dataset, valid_labels = reformat(valid_dataset, valid_labels)
test_dataset, test_labels = reformat(test_dataset, test_labels)
print('Training set', train_dataset.shape, train_labels.shape)
print('Validation set', valid_dataset.shape, valid_labels.shape)
print('Test set', test_dataset.shape, test_labels.shape)

graph = tf.Graph()
with graph.as_default():
	print("starting the graph")
	# Input data.
	tf_train_dataset = tf.placeholder(
	  tf.float32, shape=(batch_size, image_size, image_size, num_channels))
	tf_train_labels = tf.placeholder(tf.float32, shape=(batch_size, num_labels))
	tf_valid_dataset = tf.constant(valid_dataset)
	tf_test_dataset = tf.constant(test_dataset)

	# Variables.
	if model_sml:
		print("Using small model")
		layer1_weights = tf.Variable(tf.truncated_normal(
			[patch_size, patch_size, num_channels, depth], stddev=0.1))
		layer1_biases = tf.Variable(tf.zeros([depth]))
		layer2_weights = tf.Variable(tf.truncated_normal(
			[patch_size, patch_size, depth, depth], stddev=0.1))
		layer2_biases = tf.Variable(tf.constant(1.0, shape=[depth]))
		layer3_weights = tf.Variable(tf.truncated_normal(
			[image_size // 4 * image_size // 4 * depth, num_hidden], stddev=0.1))
		layer3_biases = tf.Variable(tf.constant(1.0, shape=[num_hidden]))
		layer4_weights = tf.Variable(tf.truncated_normal(
			[num_hidden, num_labels], stddev=0.1))
		layer4_biases = tf.Variable(tf.constant(1.0, shape=[num_labels]))

	else:
		layer1_weights = tf.Variable(tf.truncated_normal(
			[patch_size, patch_size, num_channels, depth], stddev=0.045))
		layer1_biases = tf.Variable(tf.zeros([depth]))
		layer2_weights = tf.Variable(tf.truncated_normal(
			[patch_size, patch_size, depth, depth], stddev=0.045))
		layer2_biases = tf.Variable(tf.constant(1.0, shape=[depth]))
		layer3_weights = tf.Variable(tf.truncated_normal(
			[image_size / 4 * image_size / 4 * depth, H1_layer_size], stddev=0.045))
		layer3_biases = tf.Variable(tf.constant(1.0, shape=[H1_layer_size]))
		layer4_weights = tf.Variable(tf.truncated_normal(
			[H1_layer_size , num_labels], stddev=0.1))
		layer4_biases = tf.Variable(tf.constant(1.0, shape=[num_labels]))

		l2_penalty = beta*(tf.nn.l2_loss(layer1_weights) +
					  tf.nn.l2_loss(layer2_weights) +
					  tf.nn.l2_loss(layer3_weights) +
					  tf.nn.l2_loss(layer4_weights) 
					 )


	# Training computation.
	logits = model(tf_train_dataset,True)
	loss = tf.reduce_mean(
	  tf.nn.softmax_cross_entropy_with_logits(logits, tf_train_labels)) 
	if not model_sml:
		loss += l2_penalty
	  
	# Optimizer.
	if model_sml:
		optimizer = tf.train.GradientDescentOptimizer(0.05).minimize(loss)
	else:
		global_step = tf.Variable(0)  # count the number of steps taken.
		learning_rate = tf.train.exponential_decay(0.01, global_step, 3000,0.95,staircase=True)
		optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss, global_step=global_step)

	# Predictions for the training, validation, and test data.
	train_prediction = tf.nn.softmax(logits)
	valid_prediction = tf.nn.softmax(model(tf_valid_dataset,False))
	test_prediction = tf.nn.softmax(model(tf_test_dataset,False))




with tf.Session(graph=graph) as session:

#gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.01)
#with tf.Session(graph=graph, config=tf.ConfigProto(log_device_placement=True, gpu_options=gpu_options)) as session:
	with tf.device('/cpu:0'):
		tf.initialize_all_variables().run()
		print('Initialized')
		for step in range(num_steps):
			offset = (step * batch_size) % (train_labels.shape[0] - batch_size)
			batch_data = train_dataset[offset:(offset + batch_size), :, :, :]
			batch_labels = train_labels[offset:(offset + batch_size), :]
			feed_dict = {tf_train_dataset : batch_data, tf_train_labels : batch_labels}
			_, l, predictions = session.run(
			  [optimizer, loss, train_prediction], feed_dict=feed_dict)
			if (step % 50 == 0):
				print('Minibatch loss at step %d: %f' % (step, l))
				print('Minibatch accuracy: %.1f%%' % accuracy(predictions, batch_labels))
				print('Validation accuracy: %.1f%%' % accuracy(valid_prediction.eval(), valid_labels))
		print('Test accuracy: %.1f%%' % accuracy(test_prediction.eval(), test_labels))




